import React from 'react';
import { MonacoDiffEditor } from 'react-monaco-editor';

class Diff extends React.Component {
  render() {
    const code1 = "// your ihio code...\n kkb";
    const code2 = "// your original code...";
    const options = {
      //renderSideBySide: false
    };
    return (
      <MonacoDiffEditor
        width="800"
        height="600"
        language="javascript"
        original={code1}
        value={code2}
        options={options}
      />
    );
  }
}

export default Diff;