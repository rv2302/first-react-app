import { Breadcrumb, Divider, Layout, Menu ,Button} from 'antd';
import React from 'react';
import DiffArea from './DiffArea';
import {Method,SearchBox,V2APISearchBox,V3APISearchBox} from './SearchBox';
import {
 UserOutlined
} from '@ant-design/icons';
import Icon from '@ant-design/icons';
import { Image } from 'antd';


require.config({ paths: { 'vs': 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.10.1/min/vs' }});

const { Header, Content, Footer } = Layout;

const DiffToolLayout = () => (
  <Layout>
    <Header
      style={{
        position: 'fixed',
        zIndex: 1,
        width: '100%',
        color:"#FFFFFF",
        textAlign:'center'
        
      }}
    >JSON DIFF GENERATOR
      <UserOutlined style={{color: "#FFFFFF",marginLeft:'1500px',fontSize:'200%'}} />
    </Header>
    
 
    <Content
      className="site-layout"
      style={{
        padding: '30px 50px',
        marginTop: 64
      }}
    >
    <span><Method style={{marginLeft:'500px',width:'5%'}}/></span> 
    <span><SearchBox/></span>
    <span><Button type="primary" size="large" onClick="" marginLeft='1000px' shape='round'> Find Diff </Button></span>
    <Icon component={() => (<img src="/home/dhanu-pt4915/Downloads/git diff.png" />)} />

    <br></br>
    <br></br>

    <V2APISearchBox></V2APISearchBox>
    <Divider style={{width:'12%'}} type="vertical"/>
    <V3APISearchBox></V3APISearchBox>
    
    <DiffArea/>
    <br></br>

    </Content>
    <Footer
      style={{
        textAlign: 'center',
      }}
    >
     
    </Footer>
  </Layout>
);

export default DiffToolLayout;