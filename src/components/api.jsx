import React, { Component, useEffect, useState } from "react"
import { Select } from 'antd';
const { Option } = Select;

const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  
  const onSearch = (value) => {
    console.log('search:', value);
  };

const  Resources = () => {
  const [resource, setResource] = useState([])

  const fetchData = () => {
    fetch("http://localhost:4000/db")
      .then(response => {
         return response.json();
      })
      .then(data => {
        setResource(data["resources"])
      })
  }

  useEffect(() => {
    fetchData()
  }, [])


  var res = (
    <>
      {resource.length > 0 && (
        <Select  
        showSearch
        placeholder="Select a resource"
        optionFilterProp="children"
        onChange={onChange}
        onSearch={onSearch}
        style={{width:'74%', marginLeft:'10px',marginRight:'100px'}}
        filterOption={(input, option) => option.children.includes(input)}
        >

          {resource.map(resource => (
              
            <Option id={resource.id} value={resource.name}> {resource.name} </Option>
          ))}
        </Select>
      )}
    </>
  )

  return res;
}

export default Resources