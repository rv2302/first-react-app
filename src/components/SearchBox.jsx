import { Select } from 'antd';
import React, { Component } from 'react';
import  Resources  from './api';
const { Option } = Select;

const onChange = (value) => {
  console.log(`selected ${value}`);
};

const onSearch = (value) => {
  console.log('search:', value);
};


const Method = () => (
    <Select
      showSearch
      placeholder="Select a Method"
      optionFilterProp="children"
      onChange={onChange}
      onSearch={onSearch}
      style={{width:'10%'}}
      filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
    >
    <Option value="get">GET</Option>
    <Option value="post">POST</Option>
    <Option value="put">PUT</Option>
    </Select>   
  );

  console.log(Method);
const SearchBox = () => (
  
    <Resources/>

);

class V2APISearchBox extends React.Component {
    render() { 
        return   <Select
        showSearch
        placeholder="Select a version"
        optionFilterProp="children"
        onChange={onChange}
        onSearch={onSearch}
        style={{width:'10%'}}
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
      >
        <Option value="jack">v2.0</Option>
        <Option value="lucy">v2.1</Option>
        <Option value="tom">v2.2</Option>
        <Option value="tom">v3.0</Option>
      </Select>;
    }
}
 
class V3APISearchBox extends React.Component {
    render() { 
        return   <Select
        showSearch
        placeholder="Select a version"
        optionFilterProp="children"
        onChange={onChange}
        onSearch={onSearch}
        style={{width:'10%',marginLeft:"450px"}}
       filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
      >
        <Option value="jack">v2.0</Option>
        <Option value="lucy">v2.1</Option>
        <Option value="tom">v2.2</Option>
        <Option value="tom">v3.0</Option>
      </Select>;
    }
}

export {Method,SearchBox,V2APISearchBox,V3APISearchBox};