import React, { Component, useEffect, useState } from "react"
import { Select } from 'antd';
const { Option } = Select;


const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  
  const onSearch = (value) => {
    console.log('search:', value);
  };

const  Methods = () => {
  const [url, setUrl] = useState([])
  const [operation, setOperation] = useState([])

  const fetchData = () => {
    fetch("http://localhost:4044/db")
      .then(response => {
         return response.json();
      })
      .then(data => {
        setUrl(data["url"])
      })
      .then(data =>{
        setOperation(data["operation"])
      })
  }

  useEffect(() => {
    fetchData()
    
  }, [])


  var res = (
    <>
      {url.length > 0 && (
        <Select  
        showSearch
        placeholder="Select a person"
        optionFilterProp="children"
        onChange={onChange}
        onSearch={onSearch}
        style={{width:'36%'}}
        filterOption={(input, option) => option.children.includes(input)}
        >

          {operation.map(operation => (
              
            <Option value={operation.method}> {operation.method} </Option>
          ))}
        </Select>
      )}
    </>
  )

  return res;
}

export default Methods