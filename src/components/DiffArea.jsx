import React, { Component } from 'react';
import {Input,Divider} from 'antd';
import { monaco, MonacoDiffEditor } from 'react-monaco-editor'; 


const {TextArea} = Input;
var code1 = `{"portal_invite": [{"code": "","details": {"record_id": "1496449000000728008" },
            "message": "An Invite has been sent to the personality.",
            "status": "success"
        }
    ]
}`;
var code2 = `{
    "portal_invite": [
        {   "code": "SUCCESS",
            "details": {
                "record_id": "1496449000000728008"
            },
            "message": "An Invite has been sent to the personality.",
            "status": "success"
        }
    ]
}`;


 code1 = JSON.stringify(JSON.parse(code1),null,'\t');
 code2 = JSON.stringify(JSON.parse(code2),null,'\t');

 console.log(code1);
 console.log(code2);
const options = {
    // renderSideBySide: true
};
class DiffArea extends React.Component {
    render() { 
        return <>  <br/>
        <MonacoDiffEditor
        width="100%"
        height="600"
        language="json"
        original={code1}
        value={code2}
        options={options}
      />
        
        </>;
    }
}
 
export default DiffArea;

