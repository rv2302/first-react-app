import React, { Component } from 'react';
import {Button} from 'antd';

class PrimaryButton extends React.Component {
    render() { 
        return <><Button type="primary" size="large"> Primary </Button></>;
    }
}


class DefaultButton extends React.Component {
    render() { 
        return <Button type="default" size="large"> Default</Button>;
    }
}
 
class Link extends React.Component {
    render() { 
        return <Button type="link" size="large">click here</Button>;
    }
}

class GhostButton extends React.Component{
    render(){
        return <Button type="Ghost" size="large">Ghost</Button>;
    }
}

export {PrimaryButton,DefaultButton,Link,GhostButton};

